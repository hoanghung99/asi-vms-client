import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ChildComponent } from '../child/child.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  constructor() { }
  
  @Input() childMessage: string;
  location: any;

  parentMessage: string = 'message from parent';

  ngOnInit() {
    this.location = {
      latitude: -28.68352,
      longitude: -147.20785
  }
  }
}

interface Location {
  latitude: number;
  longitude: number
}
